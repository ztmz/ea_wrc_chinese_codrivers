请设置steam中WRC的启动参数：“ -fileopenlog”（不带引号，目的是允许游戏自动加载补丁）<br/>
请设置游戏领航员语音为“German”（德语）<br/>
如需卸载，删除EA SPORTS WRC\WRC\Content\Paks\LaoWang_P.pak即可

中文路书文本翻译：DR2车手老王、JeffreyDeng（定速拉力部分）<br/>
中文路书翻译指导：无状态车手、bigboxx<br/>
中文真人语音包录制及编辑：DR2车手老王<br/>
程序及打包：沧竹烽<br/>
AI支持：草帽不是猫

另特别感谢以下QQ群：“ZTMZ Next Generation Pacenote”、“ZTMZ-Rally-Club总群”、“ZTMZ-Rally-Club中文路书工作组”、“RBR拉力 Simrallycn中国总群”中给大力支持免费劳动的各位群友，因为人数过多，不再一一列出。

最后本次还有参与ZTMZ外挂中文路书项目的一干人等，如草帽不是猫，以及“ZTMZ-Rally-Club中文路书工作组”中的大部分群友，他们也战斗了近一个月，虽然最终因为EAWRC的UDP问题，暂时外挂方案未能完成，但是也给与了本次内置补丁很多帮助跟思路。希望他们早日成功！

前期外挂路书项目人员：<br/>
程序：草帽不是猫<br/>
AI硬件支持：DR2车手老王<br/>
地图数据录制：DR2车手老王、沧竹烽、含蓄、年轮<br/>
地图脚本人工编制校对：DR2老王、Erwachsene、Nekuz、Panda2Bad、Sebastian、Xhown、Yang、冰彻天空、花腩居士、李昕珂、凌绝、米酷二二三、牛爷爷。